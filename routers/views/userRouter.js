const { Router } = require('express');
const express = require('express');
const router = require('express').Router();

const { UserGame } = require('../../models');
const { UserGameBiodata } = require('../../models');

// list users
router.get('/user/list', (req, res) => {
    UserGame.findAll().then(users => {
        res.render('admin/user-view', { users })
    })
});

// user register
router.get('/user/register', (req, res) => {
    let message = req.query.err

    switch (message) {
        case 'true':
            message = "username already exists"
            break
        case 'spc':
            message = "password cannot contain space!"
            break
        case 'empty':
            message = "field cannot be empty!"
            break
        default:
            message = ''
    }

    res.status(200).render('admin/user-register', {
        message
    })
});

// user login
router.get('/user/login', (req, res) => {
    let message = req.query.err
    switch (message) {
        case 'true':
            message = "wrong username/password!"
            break
        case 'empty':
            message = "field cannot be empty!"
            break
        default:
            message = ''
    }

    res.status(200).render('admin/user-login', {
        message
    })
});

// view user biodata
router.get('/user/:id/biodata', (req, res) => {

    UserGameBiodata.findOne({
        where: { user_id: req.params.id }
    }).then(user => {
        if (user === null) {
            UserGameBiodata.create({
                user_id: req.params.id,
                fname: "empty",
                lname: "empty",
                address: "empty"
            })
        }
        res.render('admin/user-biodata', { user })
    })
});

// view user biodata
router.get('/user/:id/history', (req, res) => {
    res.render('admin/user-history')
})


module.exports = router