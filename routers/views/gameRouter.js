const router = require('express').Router();
const express = require('express');

router.get('/game', (req, res) => {
    res.status(200).render('game')
});

module.exports = router