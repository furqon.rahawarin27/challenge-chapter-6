const express = require('express');
const router = require('express').Router();
const { UserGame } = require('../../models')
const { UserGameBiodata } = require('../../models');


//create new user
router.post('/api/user/create', (req, res) => {
    // Validasi!
    const {
        user_name,
        password
    } = req.body


    if (user_name === undefined || password === undefined) {
        return res.status(400).json("Beberapa field kosong!")
    } else if (user_name === '' || password === '') {
        res.status(400).redirect('/user/register?err=empty')
        return
    }

    for (let i = 0; i < password.length; i++) {
        if (password[i] === ' ') {
            res.status(400).redirect('/user/register?err=spc')
            return
        }
    }

    UserGame.findOne({
        where: {
            user_name: req.body.user_name
        }
    }).then(user => {
        if (user !== null) {
            res.status(400).redirect('/user/register?err=true')
            return
        } else {
            UserGame.create({
                user_name: user_name,
                password: password
            }).then(user => {
                res.status(200).redirect('/user/list')
            }).catch(err => {
                console.log(err)
                res.status(500).json("Internal Server Error")
            })
        }
    })


});

//view all users
router.get('/api/user', (req, res) => {
    UserGame.findAll().then(users => {
        res.json(users)
    })
});

// delete user data
router.post('/api/user/delete/:id', (req, res) => {
    UserGame.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => {
        res.status(200).render('admin/user-success')
    }).catch(err => {
        console.log(err)
        res.status(500).json("Internal server error")
    })

});

// user login
router.post('/api/user/login', (req, res) => {
    const {
        user_name,
        password
    } = req.body

    if (user_name === undefined || password === undefined) {
        return res.status(400).json("Beberapa field kosong!")
    } else if (user_name === '' || password === '') {
        res.status(400).redirect('/user/login?err=empty')
        return
    }

    UserGame.findOne({
        where: {
            user_name: req.body.user_name,
            password: req.body.password
        }
    }).then(user => {
        if (user === null) {
            res.status(400).redirect('/user/login?err=true')
            return
        } else {
            res.redirect(301, '/user/list')
        }
    })

});

// update user biodata
router.post('/api/user/:id/biodata/edit', (req, res) => {

    UserGameBiodata.findOne({
        where: {
            user_id: req.params.id
        }
    }).then(user => {
        user.update({
            fname: req.body.fname,
            lname: req.body.lname,
            address: req.body.address
        })
        res.redirect(`/user/${req.params.id}/biodata`)
    }
    )
});



module.exports = router