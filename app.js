// imports
const express = require('express');
const { UserGame } = require('./models')
const app = express()
const port = 8000

// import middleware
app.use(express.json())
app.use(express.urlencoded({
    extended:false
}))

// import routers
app.use(require('./routers/views/homeRouter'))
app.use(require('./routers/views/gameRouter'))
app.use(require('./routers/api/userRouter'))
app.use(require('./routers/views/userRouter'))

// static files
app.use(express.static('assets'))
app.use('/css', express.static(__dirname + '/assets/css'))
app.use('/js', express.static(__dirname + '/assets/js'))
app.use('/images', express.static(__dirname + '/assets/images'))

//set views
app.set('views', './views')
app.set('view engine', 'ejs')


app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});